package main

import (
    "fmt"
    "math"
)

func absInt(a int8) int8 {
    if a < 0 {
        return -a
    } else {
        return a
    }
}

func mul (a,b int8) (int8, bool) {

    if a > b {
        a, b = b, a
    }

    if (a > 0 && b > 0) || (a < 0 && b < 0) {
        if math.MaxInt8 / absInt(a) >= absInt(b) {
            return a * b, true
        }
    } else {
        if math.MinInt8 / a >= b {
            return a * b, true
        }
    }

    return 0, false
}

func main() {
    fmt.Println(mul(4,2))
    fmt.Println(mul(8,16))
    fmt.Println(mul(-10,10))
    fmt.Println(mul(-16,8))
    fmt.Println(mul(-16,9))
    fmt.Println(mul(-16,-10))
    fmt.Println(mul(16,10))
}