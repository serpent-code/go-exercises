package main

import "fmt"

func newEvenNumberGenerator(max int) func() (int, bool) {

    // a is set to -2 at start to be able to return the initial 0, true
    a := -2
    b := true

    return func() (int, bool) {
        if a + 2 < max {
            a += 2
            b = true
        } else {
            a = 0
            b = false
        }
        return a, b
    }
}



func main() {

    nextEvenNumber := newEvenNumberGenerator(19)
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())
    fmt.Println(nextEvenNumber())

}