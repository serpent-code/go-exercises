package main

import "fmt"

func min(num1 int, num2 int, nums ...int) int {
// num1 and num2 are only added explicitly because of the requirement that the function should receive at least 2 arguments otherwise it should be compiler error.
// checking for len(nums) and panicing would be a run time error and not what we want.
// The min and max uses simple high water mark algorithm.
// Not using a separate slice to collect all arguments might make it a bit ugly (checking num1 and num2 separately) but at least we iterate over numbers only once.

    a := num1

    if num2 < num1 {
        a = num2
    }

    for _, num := range nums {
        if num < a {
            a = num
        }
    }

    return a

}

func max(num1 int, num2 int, nums ...int) int {

    a := num1

    if num2 > num1 {
        a = num2
    }

    for _, num := range nums {
        if num > a {
            a = num
        }
    }

    return a

}



func main() {
    fmt.Println(min(1,2))
    fmt.Println(min(-200, -100, 0, 100, 200))
    //fmt.Println(min(1))
    fmt.Println(max(1,2))
    fmt.Println(max(-200, -100, 0, 100, 200))
    //fmt.Println(max(1))

}