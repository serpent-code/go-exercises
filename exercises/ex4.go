package main

import (
    "strings"
    "fmt"
)

func joinStrings(input_strings ...string) string {
    // https://stackoverflow.com/questions/1760757/how-to-efficiently-concatenate-strings-in-go

    var sb strings.Builder

    separator := input_strings[0]

    for _, str := range input_strings[1:] {
        sb.WriteString(str)
        sb.WriteString(separator)
    }
    // not sure about efficiency of this however
    return sb.String()[:sb.Len()-1]
}


func main() {
    fmt.Println(joinStrings(",", "foo","bar"))
    fmt.Println(joinStrings(",", "a"))
    fmt.Println(joinStrings(",", "a", "b"))
    fmt.Println(joinStrings(",", "a", "b", "c"))
}
